## Interface: 110007
## Title: BuffFrame
## Notes: Tracks your buffs with dragable frames.
## DefaultState: enabled
## Author: Josh Kelm
## Version: 2.6.3
## SavedVariablesPerCharacter: buffFrameIsLocked, trackedBuffs, iconSize

BuffFrame.lua
BuffFrame.xml