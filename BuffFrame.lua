local addonName = "BuffFrame";
local uiFrames = {};

-- Handles slash commands --
function handleSlashCommand(msg, editbox)
	if msg == "unlock" then
		print("BuffFrame is unlocked.");
		buffFrameIsLocked = false;
		local framesLockedOption = _G["options_toggleFramesLocked"];
		if (framesLockedOption ~= nil) then
			framesLockedOption:SetChecked(false);
		end
		updateClickthroughOnFrame();
		checkForBuff();
	elseif msg == "lock" then
		print("BuffFrame is locked.");
		buffFrameIsLocked = true;
		local framesLockedOption = _G["options_toggleFramesLocked"];
		if (framesLockedOption ~= nil) then
			framesLockedOption:SetChecked(true);
		end
		updateClickthroughOnFrame();
		checkForBuff();
	elseif msg == "clear" then
		print("Tracked buffs have been cleared");
		trackedBuffs = {};
		uiFrames = {};
	elseif msg == "list" then
		printTrackedBuffs();
	elseif msg == "options" or msg == "o" or msg == "config" or msg == "c" then
		InterfaceOptionsFrame_OpenToCategory("BuffFrame");
	elseif msg:find("^remove") ~= nil then
		local buffNameToRemove = string.sub(msg, 8);
		removeTrackedBuff(buffNameToRemove);
	elseif msg ~= "" then
		trackBuff(msg);
		printTrackedBuffs();
	else
		print("Usage:");
		print("    '/bf unlock' - will let you drag frames you are tracking");
		print("    '/bf lock' - will lock frames in place");
		print("    '/bf clear' - will remove all tracked buffs");
		print("    '/bf list' - will print a list all buffs being tracked");
		print("    '/bf remove <buffName>' - will remove buffName from buffs being tracked");
		print("    '/bf <buffName>' - will track buffName");
		print("    '/bf options|config|o|c' - will open built-in options page");
		print("    '/bf' - prints this usage menu");
	end
end

-- Receives OnEvent calls for events that this addon is registered for. --
-- In this case it handles checking for the specified buff. --
function handleGeneralEvent(self, event, ...)
	if event == "UNIT_AURA" then
		local target = ...;
		if target == "player" then
			checkForBuff();
		end
	elseif event == "ADDON_LOADED" then
		local loadedAddon = ...;
		if loadedAddon == addonName then
			if buffFrameIsLocked == nil then
				buffFrameIsLocked = false;
			end
			if iconSize == nil then
				iconSize = 24;
			end
			if trackedBuffs == nil then
				trackedBuffs = {};
			else
				createTrackedFrames();
			end
			SLASH_BUFFFRAME1 = '/bf';
			SlashCmdList['BUFFFRAME'] = handleSlashCommand;
			configureOptions();
			checkForBuff();
		end
	elseif event == "PLAYER_SPECIALIZATION_CHANGED" then
		createTrackedFrames();
		checkForBuff();
	end
end

-- Configures all the UI fields for the built-in WoW Addon options. --
function configureOptions()
	local configFrame = CreateFrame("Frame", "configFrame");
	configFrame.name = "BuffFrame";
	if InterfaceOptions_AddCategory then
		InterfaceOptions_AddCategory(configFrame);
	else
		local category, layout = Settings.RegisterCanvasLayoutCategory(configFrame, configFrame.name);
		Settings.RegisterAddOnCategory(category);
	end

	-----------------------
	-- General Options
	local header = configFrame:CreateFontString(nil, "ARTWORK","GameFontNormalLarge");
	header:SetPoint("TOPLEFT", 10, -10);
	header:SetText("Options");

	local toggleFramesLocked = CreateFrame("CheckButton", "options_toggleFramesLocked", configFrame, "ChatConfigCheckButtonTemplate");
	toggleFramesLocked:SetPoint("TOPLEFT", 10, -40);
	toggleFramesLocked:SetChecked(buffFrameIsLocked);
	_G[toggleFramesLocked:GetName() .. "Text"]:SetText("Lock BuffFrames");
	toggleFramesLocked.tooltip = "Control whether buff frames can be moved around the screen";
	toggleFramesLocked:SetScript("OnClick", function(self)
		buffFrameIsLocked = not buffFrameIsLocked;
		updateClickthroughOnFrame();
		checkForBuff();
	end);
	
	local iconSizeSliderValue = configFrame:CreateFontString(nil, "ARTWORK","GameFontNormalSmall");
	local iconSizeSlider = CreateFrame("Slider", "options_iconSizeSlider", configFrame, "OptionsSliderTemplate");
	_G[iconSizeSlider:GetName() .. "Text"]:SetText("Icon Size");
	_G[iconSizeSlider:GetName() .. "Low"]:SetText("16");
	_G[iconSizeSlider:GetName() .. "High"]:SetText("64");
	iconSizeSlider:SetPoint("TOPLEFT", 200, -55)
	iconSizeSlider:SetOrientation("HORIZONTAL");
	iconSizeSlider:SetMinMaxValues(16, 64);
	iconSizeSlider:SetValueStep(8);
	iconSizeSlider:SetValue(iconSize);
	iconSizeSlider:SetObeyStepOnDrag(true);
	iconSizeSlider:SetScript("OnValueChanged", function(self)
		iconSize = iconSizeSlider:GetValue();
		iconSizeSliderValue:SetText(iconSize);
		updateTrackedFrameSizes();
	end);
	iconSizeSliderValue:SetPoint("TOPLEFT", 305, -44);
	iconSizeSliderValue:SetText(iconSize);
	
	-----------------------
	-- Tracked buffs
	local scrollFrameHeader = configFrame:CreateFontString(nil, "ARTWORK","GameFontNormalLarge");
	scrollFrameHeader:SetPoint("TOPLEFT", 10, -90);
	scrollFrameHeader:SetText("Tracked Buffs");

	local scrollFrame = CreateFrame("ScrollFrame", "options_scrollFrame", configFrame, "UIPanelScrollFrameTemplate");
	scrollFrame:SetSize(250, 200);
	scrollFrame:SetPoint("TOPLEFT", 10, -120);
	scrollFrame.buttons = {};
	scrollFrame.buttonHeight = 19;
	scrollFrame.selectedButton = nil;
	scrollFrame.normalTexture = [[Interface\Blues\COMMON\Glue-Panel-Button-Disabled]]
	scrollFrame.highlightTexture = [[Interface\Buttons\UI-Listbox-Highlight]];

	local editBox = CreateFrame("EditBox", "options_editBox", configFrame, "InputBoxTemplate");
	editBox:SetAutoFocus(false);
	editBox:SetSize(245, 32);
	editBox:SetPoint("BOTTOMLEFT", scrollFrame, 5, -30);

	local scrollContent = CreateFrame("Frame", "options_scrollContent", scrollFrame, "InsetFrameTemplate3");
	for i = 1, #trackedBuffs do
		updateScrollContent();
	end

	local addButton = CreateFrame("Button", nil, configFrame, "UIPanelButtonTemplate");
	addButton:SetText("Add");
	addButton:SetScript("OnClick", function(self)
		trackBuff(editBox:GetText());
		editBox:SetText("");
		editBox:SetFocus(true);
	end);
	addButton:SetSize(60, 25);
	addButton:SetPoint("BOTTOMLEFT", editBox, -6, -20);

	local removeButton = CreateFrame("Button", nil, configFrame, "UIPanelButtonTemplate");
	removeButton:SetText("Remove");
	removeButton:SetScript("OnClick", function(self)
		removeTrackedBuff(editBox:GetText());
		scrollFrame.selectedButton = nil;
		editBox:SetText("");
		editBox:SetFocus(true);
		scrollFrame:SetVerticalScroll(0);
	end);
	removeButton:SetSize(60, 25);
	removeButton:SetPoint("RIGHT", addButton, 60, 0);

end

-- Updates the list of tracked buffs, adding new ones if a buff was added. --
function updateScrollContent()
	
	local scrollFrame = _G["options_scrollFrame"];
	local scrollContent = _G["options_scrollContent"];
	local editBox = _G["options_editBox"];

	for i = 1, #trackedBuffs do
		local button = _G["options_trackedBuffsItem" .. i];
		if (button == nil) then
			button = CreateFrame("Button", "options_trackedBuffsItem" .. i, scrollContent, "BackdropTemplate,UIDropDownMenuButtonTemplate");
		end
		_G[button:GetName() .. "Check"]:Hide();
		_G[button:GetName() .. "UnCheck"]:Hide();
		if i == 1 then
			button:SetPoint("TOPLEFT", scrollContent, 10, -4);
		else
			button:SetPoint("TOPLEFT", scrollFrame.buttons[i - 1], "BOTTOMLEFT");
		end

		button:SetText(trackedBuffs[i].name .. " (" .. trackedBuffs[i].spec .. ")");
		button:SetSize(scrollFrame:GetWidth(), scrollFrame.buttonHeight);
		button:ClearNormalTexture();
		button:SetNormalTexture(scrollFrame.normalTexture);
		button:ClearHighlightTexture();
		button:SetHighlightTexture(scrollFrame.highlightTexture);
		button:Show();
		button:SetScript("OnClick", function(self)
			scrollFrame.selectedButton = self;
			editBox:SetFocus(true);
			editBox:SetText(trackedBuffs[i].name);
			clearButtonHighlights();
			self:SetNormalTexture(scrollFrame.highlightTexture);
		end)

		scrollFrame.buttons[i] = button;
	end

	-- If we have more buttons than tracked buffs, hide the excess
	if #scrollFrame.buttons > #trackedBuffs then
		local diff = #scrollFrame.buttons - #trackedBuffs;
		for i = #trackedBuffs + 1, #trackedBuffs + diff do
			scrollFrame.buttons[i]:Hide();
		end

	end

	local contentHeight = #trackedBuffs * scrollFrame.buttonHeight + 5;
	if contentHeight < scrollFrame:GetHeight() then
		contentHeight = scrollFrame:GetHeight();
	end
	scrollContent:SetSize(scrollFrame:GetWidth(), contentHeight);
	scrollContent:SetPoint("BOTTOMLEFT", scrollFrame, "BOTTOMLEFT");
	scrollFrame:SetScrollChild(scrollContent);
end

-- Clears the highlight state of all the buttons in the tracked buff config list. --
function clearButtonHighlights()
	local scrollFrame = _G["options_scrollFrame"];
	for i = 1, #trackedBuffs do
		local button = _G["options_trackedBuffsItem" .. i];
		button:ClearNormalTexture();
		button:SetNormalTexture(scrollFrame.normalTexture);
	end
end

-- Steps through each tracked buff and creates a UI frame for it. --
function createTrackedFrames()
	uiFrames = {};
	for i = 1, #trackedBuffs do
		local trackedBuff = trackedBuffs[i];
		uiFrames[trackedBuff.name] = CreateFrame("Frame", nil, UIParent, "buffFrameTemplate");
		local frame = uiFrames[trackedBuff.name];
		frame:SetSize(iconSize, iconSize);
		frame:EnableMouse(not buffFrameIsLocked)
		frame.buffDuration:SetFontObject(_G["customBuffFrameFont" .. iconSize]);
		frame.buffCount:SetFontObject(_G["customBuffFrameFont" .. iconSize]);
		if trackedBuff.xOffset and trackedBuff.yOffset then
			frame:ClearAllPoints();
			frame:SetPoint(trackedBuff.point, UIParent, trackedBuff.relativePoint, trackedBuff.xOffset, trackedBuff.yOffset);
		end
	end
end

-- Steps through each tracked buff updates the size of the frame. --
function updateTrackedFrameSizes()
	for i = 1, #trackedBuffs do
		local trackedBuff = trackedBuffs[i];
		local frame = uiFrames[trackedBuff.name];
		if (frame ~= nil) then
			frame:SetSize(iconSize, iconSize);
			frame.buffDuration:SetFontObject(_G["customBuffFrameFont" .. iconSize]);
			frame.buffCount:SetFontObject(_G["customBuffFrameFont" .. iconSize]);
		end
	end
end

-- Steps through each tracked buff frame and allows it to be clicked through if the addon is locked. --
function updateClickthroughOnFrame()
	for i = 1, #trackedBuffs do
		local trackedBuff = trackedBuffs[i];
		local frame = uiFrames[trackedBuff.name];
		if (frame ~= nil) then
			frame:EnableMouse(not buffFrameIsLocked);
		end
	end
end

-- Calls AuraUtil for the specified buff name and sets flags based on the buff. --
-- Also updates the frame icon and text if the buff is found. --
function checkForBuff()
	if not GetSpecialization() then
		return;
	end
	local spec = select(2, GetSpecializationInfo(GetSpecialization()));
	for i = 1, #trackedBuffs do
		local trackedBuff = trackedBuffs[i];
		local frame = uiFrames[trackedBuff.name];
		if frame and trackedBuff.spec == spec then
			if not buffFrameIsLocked then
				frame.buffDuration:SetText(trackedBuff.name);
				frame.buffIcon:SetTexture("Interface/Tooltips/UI-Tooltip-Background");
				frame.buffCount:SetText(nil);
			else
				local buffName, icon, count, debuffType, duration, expirationTime = AuraUtil.FindAuraByName(trackedBuff.name, "player");
				if not icon and tonumber(trackedBuff.name) ~= nil then
					local aura = C_UnitAuras.GetPlayerAuraBySpellID(tonumber(trackedBuff.name));
					if aura then
						buffName = aura.name;
						icon = aura.icon;
						count = not nil and aura.charges or 0;
						duration = aura.duration;
						expirationTime = aura.expirationTime;
					end
				end

				frame.buffName = buffName;
				if icon then
					if duration == 0 then
						frame.buffDuration:SetText(nil);
					else
						frame.buffDuration:SetText(formatTime(expirationTime - GetTime()));
					end				
					if count > 1 then
						frame.buffCount:SetText(count);
					else
						frame.buffCount:SetText(nil);
					end
					frame.buffIcon:SetTexture(icon);
					trackedBuff.hasBuff = true;
				else
					frame.buffDuration:SetText(nil);
					frame.buffIcon:SetTexture(nil);
					frame.buffCount:SetText(nil);
					trackedBuff.hasBuff = false;
				end
			end
		end
	end
end

-- Handler for the OnUpdate event. Handles updating the duration of the buff on screen. --
function handleUpdateEvent(self, elapsed)
	for i = 1, #trackedBuffs do
		local trackedBuff = trackedBuffs[i];
		local frame = uiFrames[trackedBuff.name];
		if frame and trackedBuff.hasBuff then
			local buffName, icon, count, debuffType, duration, expirationTime = AuraUtil.FindAuraByName(trackedBuff.name, "player");
			if expirationTime then
				local remainingSec = formatTime(expirationTime - GetTime());
				if duration == 0 then
					frame.buffDuration:SetText(nil);
				else
					frame.buffDuration:SetText(remainingSec);
				end
			end
		end
	end
end

-- Checks to make sure the provided buffName is not already being tracked and will start tracking it. --
-- This includes creating the frame to display the buff information. --
function trackBuff(buffName)
	local spec = select(2, GetSpecializationInfo(GetSpecialization()));
	for i = 1, #trackedBuffs do
		if trackedBuffs[i].name == buffName and trackedBuffs[i].spec == spec then
			print(buffName .. " is already being tracked");
			return;
		end
	end
	local buffToTrack = {};
	buffToTrack.hasBuff = false;
	buffToTrack.name = buffName;
	buffToTrack.spec = spec;
	uiFrames[buffToTrack.name] = CreateFrame("Frame", nil, UIParent, "buffFrameTemplate");
	table.insert(trackedBuffs, buffToTrack);
	updateScrollContent();
end

-- Removes the a buff from being tracked --
function removeTrackedBuff(buffName)
	local removedItem;
	for i = 1, #trackedBuffs do
		if trackedBuffs[i].name == buffName then
			removedItem = table.remove(trackedBuffs, i);
			updateScrollContent();
			return;
		end
	end
end

-- Formats the time to display nicely on the UI. --
function formatTime(seconds)
	if (seconds < 0) then 
		return 0; 
	end
	if (seconds > 59) then
		minuteTime = floor(seconds/60);
		secondTime = floor(seconds%60);
		if (secondTime < 10) then 
			secondTime = "0" .. secondTime; 
		end
		return minuteTime .. ":" .. secondTime;
	end

	return floor(seconds);
end

-- Moves the actual buff frame with the left mouse. --
function moveBuffFrame(self, button)
	if button == "LeftButton" and not self.isMoving and not buffFrameIsLocked then
		self:StartMoving();
		self.isMoving = true;
	end
end

-- Stops movement of the buff frame. --
function stopMoveBuffFrame(self, button)
	if buffFrameIsLocked then
		self:StopMovingOrSizing();
		self.isMoving = false;
	else
		if button == "LeftButton" and self.isMoving then
			self:StopMovingOrSizing();
			self.isMoving = false;

			-- uiFrames is an associative array where the key is buffName and value is the UI frame element
			for buffName, frame in pairs(uiFrames) do
				if self == frame then
					
					local point, relativeTo, relativePoint, xOffset, yOffset = self:GetPoint();

					for i = 1, #trackedBuffs do
						if trackedBuffs[i].name == buffName then
							trackedBuffs[i].point = point;
							trackedBuffs[i].relativeTo = relativeTo;
							trackedBuffs[i].relativePoint = relativePoint;
							trackedBuffs[i].xOffset = xOffset;
							trackedBuffs[i].yOffset = yOffset;
						end
					end
				end
			end
		end
	end
end

-- Prints the current list of tracked buffs --
function printTrackedBuffs()
	print("Tracking " .. #trackedBuffs .. " buffs");
	for i = 1, #trackedBuffs do
		print("  - " .. trackedBuffs[i].name .. " (" .. trackedBuffs[i].spec .. ")");
	end
end
